open Graphics

type pacman_tile = PacSol | PacMur
type pacman_mode = Chase | Scatter
type pacman_monde = pacman_tile array array
type pacman_direction = PacHaut | PacBas | PacGauche | PacDroite
type pacman_status = Running | TurningTo of int * int
type pacman_personnage = { mutable position : int * int;
    mutable direction : pacman_direction;
    mutable status : pacman_status }
type pacman_fantomes = pacman_personnage * pacman_personnage * pacman_personnage * pacman_personnage
type pacman_deplace = pacman_mode -> pacman_monde -> pacman_personnage -> pacman_fantomes ->
    (pacman_direction * (int * int)) list -> pacman_direction
type jeu =
    Pacman of
        ( pacman_deplace * pacman_deplace * pacman_deplace * pacman_deplace )

let input_int f =
    let b1 = input_byte f in
    let b2 = input_byte f in
    let b3 = input_byte f in
    let b4 = input_byte f in
    b4 * 16777216 + b3 * 65536 + b2 * 256 + b1

let input_word f =
    let b1 = input_byte f in
    let b2 = input_byte f in
    b2 * 256 + b1

let skip f n = seek_in f (pos_in f + n)
let open_bitmap_image_file fn =
    let f = open_in_bin fn in
    let magicB = input_char f in
    let magicM = input_char f in
    if magicB <> 'B' || magicM <> 'M' then failwith "Not a BMP file";
    skip f (4 * 2);
    let bitmap_data_offset = input_int f in
    skip f 4;
    let width = input_int f in
    let height = input_int f in
    skip f 2;
    let bpp = input_word f in
    let compression = input_int f in
    skip f (4 * 5);
    let palette = Array.make 256 black in
    for i = 0 to 255 do
        let b = input_byte f in
        let g = input_byte f in
        let r = input_byte f in
        seek_in f (pos_in f + 1);
        palette.(i) <- rgb r g b
    done;
    let pixels = Array.make_matrix width height (char_of_int 0) in
    let scanline = ((8 * width + 31)/32)*4 in
    let pad = scanline - width in
    if bpp <> 8 || compression <> 0
    then failwith "Not a valid 8bit BMP file";
    seek_in f bitmap_data_offset;
    for j = 0 to height - 1 do
        for i = 0 to width - 1 do
            pixels.(i).(j) <- input_char f
        done;
        seek_in f (pos_in f + pad)
    done;
    (pixels, palette)

type game_engine = {
    mutable frames : int;
    mutable screen_width : int;
    mutable screen_height : int;
    mutable bitmap_atlas : (int * (char array array * color array)) list;
    mutable tile_map_to_bitmap_id : (int -> int -> int option) option;
    mutable tile_width : int;
    mutable tile_height : int;
    mutable get_sprites_count : (unit -> int) option;
    mutable get_sprite : (int -> int * (int * int) * bool * bool) option;
    mutable draw_func : (unit -> unit) option;
    mutable keypress_func : (char -> unit) option;
    mutable mousedown_func: (int * int -> unit) option;
    mutable mouseup_func: (int * int -> unit) option;
    mutable mousemove_func : (int * int -> unit) option;
    mutable idle_func : (int -> unit) option;
    mutable mouse_position : int * int;
    mutable mouse_state : bool;
    mutable key_pressed : char option;
    mutable running : bool;
    mutable dirty : bool array array
    }

let wait_for_keypress () = let _ = wait_next_event [Key_pressed] in ()

let create_game_engine w h tw th =
    {
        frames = 0;
        screen_width = w;
        screen_height = h;
        bitmap_atlas = [];
        tile_map_to_bitmap_id = None;
        tile_width = tw;
        tile_height = th;
        get_sprites_count = None;
        get_sprite = None;
        draw_func = None;
        keypress_func = None;
        mousedown_func = None;
        mouseup_func = None;
        mousemove_func = None;
        idle_func = None;
        key_pressed = None;
        mouse_position = (0,0);
        mouse_state = false;
        running = true;
        dirty = Array.make_matrix (w/tw) (h/th) true
    }

let init_graphics_engine ge =
    open_graph (Printf.sprintf " %dx%d" (16+ge.screen_width) (32+ge.screen_height))

let convert_indexed_color_array_array_to_image i p =
    let w = Array.length i in
    let h = Array.length i.(0) in 
    let v = Array.make_matrix h w transp in
    for x = 0 to w-1 do
    for y = 0 to h-1 do
        v.(h-1-y).(x) <- p.(int_of_char i.(x).(y))
    done
    done; make_image v
    
let bitmap_cache = ref []
let get_bitmap ge tid =
    if List.mem_assoc tid !bitmap_cache
    then List.assoc tid !bitmap_cache
    else let b, pal = List.assoc tid ge.bitmap_atlas in
        let bi = convert_indexed_color_array_array_to_image b pal in
        bitmap_cache := (tid,bi) :: !bitmap_cache;
        bi

let display_tile ge =
    fun i j ->
    match ge.tile_map_to_bitmap_id with
    Some ft ->
        begin
        let toid = ft i j in
        match toid with
        Some tid ->
        if List.mem_assoc tid ge.bitmap_atlas then
            draw_image (get_bitmap ge tid)
                (i * ge.tile_width) (j * ge.tile_height)
        | _ -> ()
        end
    | _ -> ()

let display_tile_map ge =
    let tw = ge.screen_width / ge.tile_width in
    let th = ge.screen_height / ge.tile_height in
    for i = 0 to tw-1 do 
        for j = 0 to th-1 do
            if ge.dirty.(i).(j) then display_tile ge i j
        done
    done

(* works because our rects are aa *)
let intersect_rect (x1,y1,w1,h1) (x2,y2,w2,h2) =
    let x2in1 = x1 <= x2 && x2 < x1+w1 in
    let x1in2 = x2 <= x1 && x1 < x2+w2 in
    let y2in1 = y1 <= y2 && y2 < y1+h1 in
    let y1in2 = y2 <= y1 && y1 < y2+h2 in
    (x2in1 || x1in2) && (y2in1 || y1in2)

let mark_dirty ge x y w h =
    let tw = ge.screen_width / ge.tile_width in
    let th = ge.screen_height / ge.tile_height in
    for i = 0 to tw-1 do
        for j = 0 to th-1 do
            if intersect_rect
                (i*ge.tile_width,j*ge.tile_height,ge.tile_width,ge.tile_height)
                (x,y,w,h)
            then ge.dirty.(i).(j) <- true
        done
    done
    

let mark_dirty_animated_sprites ge =
    match (ge.get_sprites_count, ge.get_sprite) with
    Some gsc, Some gs ->
        for i = 0 to gsc () - 1 do
            let bid, (x, y), a, wcentered = gs i in
            let b, pal =List.assoc bid ge.bitmap_atlas in
            let bw = Array.length b in
            let bh = Array.length b.(0) in
            if a then 
                mark_dirty ge 
                    (if wcentered then x + (ge.tile_width-bw)/2 else x) y
                    bw bh
        done
    | _ -> ()


let clear_dirty_flags ge =
    let tw = ge.screen_width / ge.tile_width in
    let th = ge.screen_height / ge.tile_height in
    for i = 0 to tw-1 do
        for j = 0 to th-1 do
            ge.dirty.(i).(j) <- false
        done
    done
    

let display_sprites ge =
    match (ge.get_sprites_count, ge.get_sprite) with
    Some gsc, Some gs ->
        for i = 0 to gsc () - 1 do
            let bid, (x, y), a, wcentered = gs i in
            let b, pal =List.assoc bid ge.bitmap_atlas in
            let bw = Array.length b in
            let bi = get_bitmap ge bid in
            draw_image bi 
                (if wcentered then  x + (ge.tile_width-bw)/2 else x) y
        done
    | _ -> ()

let display_graphics ge =
    mark_dirty_animated_sprites ge;
    display_tile_map ge;
    display_sprites ge;
    clear_dirty_flags ge;
    match ge.draw_func with Some f -> f () | None -> (); 
    mark_dirty_animated_sprites ge

let stop_loop ge = ge.running <- false

let set_draw_func ge f = ge.draw_func <- Some f
let set_keypress_func ge f = ge.keypress_func <- Some f
let set_mousedown_func ge f = ge.mousedown_func <- Some f
let set_mouseup_func ge f = ge.mouseup_func <- Some f
let set_mousemove_func ge f = ge.mousemove_func <- Some f
let set_idle_func ge f = ge.idle_func <- Some f
let set_tile_map_to_bitmap_id ge f = ge.tile_map_to_bitmap_id <- Some f
let set_get_sprites_count ge f = ge.get_sprites_count <- Some f
let set_get_sprite ge f = ge.get_sprite <- Some f

let submatrix m x y w h =
    let sm = Array.make_matrix w h m.(x).(y) in
    for i = x to x+w-1 do
        for j = y to y+h-1 do
            sm.(i-x).(j-y) <- m.(i).(j)
        done
    done;
    sm

let load_array_of_bitmaps ge bitmapfn tw th bx by clear =
    let pixels, palette = open_bitmap_image_file bitmapfn in
    palette.(0) <- transp;
    if clear then ge.bitmap_atlas <- [];
    let first_index = List.length ge.bitmap_atlas in
    let width = Array.length pixels in
    let height = Array.length pixels.(0) in
    let ntW = width / (tw+bx) in
    let ntH = height / (th+by) in
    for i = 0 to ntW - 1 do
        for j = 0 to ntH - 1 do
            let tile = submatrix pixels
                (i * (tw+bx) + bx) (j * (th+by) + by)
                tw th in
            let id = i + (ntH - 1 - j) * ntW in
            ge.bitmap_atlas <- (first_index+id,(tile,palette))::ge.bitmap_atlas
        done
    done;
    (first_index, List.length ge.bitmap_atlas-first_index)

let load_tileset ge bitmapfn bx by = load_array_of_bitmaps ge 
    bitmapfn ge.tile_width ge.tile_height bx by true

let load_spritesheet ge bitmapfn tw th bx by =
    load_array_of_bitmaps ge bitmapfn tw th bx by false

let minimal_frame_time = 1.0 /. 60.0

let call f arg = match f with
    Some rf -> rf arg
    | _ -> ()

let main_loop ge =
    ge.running <- true;
    while ge.running do
        let start_time = Sys.time () in
        ge.frames <- ge.frames + 1;
        display_graphics ge;

        call ge.idle_func ge.frames;

        let old_mouse_state = ge.mouse_state in
        let old_mouse_position = ge.mouse_position in
        ge.mouse_position <- mouse_pos ();
        ge.mouse_state <- button_down ();

        if key_pressed ()
        then (let c = read_key () in
            ge.key_pressed <- Some c;
            call ge.keypress_func c)
        else ge.key_pressed <- None;

        if old_mouse_state <> ge.mouse_state
        then begin
            if ge.mouse_state
            then call ge.mousedown_func ge.mouse_position;
            if not ge.mouse_state 
            then call ge.mouseup_func ge.mouse_position
            end;

        if old_mouse_position = ge.mouse_position
        then call ge.mousemove_func ge.mouse_position;

        while Sys.time () < start_time +. minimal_frame_time 
        do
            ()
        done
    done


let convert_position_t2w ge (x,y) = 
    (x * ge.tile_width, y * ge.tile_height)

let convert_position_w2t ge (x,y) = 
    (x / ge.tile_width, y / ge.tile_height)

let get_frame_count ge = ge.frames
let cycle_ids ge v = v.((ge.frames/10) mod (Array.length v))

let pacman_lance (fb,fp,fi,fc) =
    let monde_largeur = 28 in
    let monde_hauteur = 32 in
    let tdim = 8 in
    let ge = create_game_engine (monde_largeur * tdim) (monde_hauteur * tdim)
            tdim tdim in
    let (base_ts, num_tiles_base) = load_tileset ge "pacman.bmp" 0 0 in
    let (base_sp, _) = load_spritesheet ge "pacman.bmp" 16 16 0 0 in
    let pacb = base_sp + 14 * (16 + 5) in
    let pac_ids = [| pacb; pacb+1; pacb+2 |] in
    let blinkyb = base_sp + 14 * (16 + 0) in
    let blinky_ids = [| blinkyb; blinkyb+1 |] in
    let pinkyb = base_sp + 14 * (16 + 2) in
    let pinky_ids = [| pinkyb; pinkyb+1 |] in
    let inkyb = base_sp + 14 * (16 + 1) in
    let inky_ids = [| inkyb; inkyb+1 |] in
    let clydeb = base_sp + 14 * (16 + 3) in
    let clyde_ids = [| clydeb; clydeb+1 |] in
    let monde = Array.make_matrix monde_largeur monde_hauteur PacMur in
    let est_sol tx ty = monde.(tx).(ty) = PacSol in
    let cases = [|
    "############################";
    "#............##............#";
    "#.####.#####.##.#####.####.#";
    "#.####.#####.##.#####.####.#";
    "#.####.#####.##.#####.####.#";
    "#..........................#";
    "#.####.##.########.##.####.#";
    "#.####.##.########.##.####.#";
    "#......##....##....##......#";
    "######.#####.##.#####.######";
    "######.#####.##.#####.######";
    "######.##..........##.######";
    "######.##.########.##.######";
    "######.##.########.##.######";
    "..........########..........";
    "######.##.########.##.######";
    "######.##.########.##.######";
    "######.##..........##.######";
    "######.##.########.##.######";
    "######.##.########.##.######";
    "#............##............#";
    "#.####.#####.##.#####.####.#";
    "#.####.#####.##.#####.####.#";
    "#...##................##...#";
    "###.##.##.########.##.##.###";
    "###.##.##.########.##.##.###";
    "#......##....##....##......#";
    "#.##########.##.##########.#";
    "#.##########.##.##########.#";
    "#..........................#";
    "############################" |] in
    for j = 0 to Array.length cases - 1 do
        for i = 0 to String.length cases.(j) - 1 do
            if cases.(j).[i] = '.'
            then monde.(i).(monde_hauteur - j - 2 ) <- PacSol
        done
    done;
    let t2w p = let x, y = convert_position_t2w ge p in x, y in
    let pac = { position = 12, 12; direction = PacDroite; status = Running } in
    let blinky = { position = 14 * 8 + 4, 19 * 8 + 4; direction = PacDroite; status = Running } in
    let inky = { position = 14 * 8 + 4, 19 * 8 + 4; direction = PacGauche; status = Running } in
    let pinky = { position = 14 * 8 + 4, 19 * 8 + 4; direction = PacGauche; status = Running } in
    let clyde = { position = 14 * 8 + 4, 19 * 8 + 4; direction = PacDroite; status = Running } in
    let perso_tile perso =
        let x, y = perso.position in
        x / tdim, y / tdim
    in
    let deplace_dir p d =
        let x, y = p in
        match d with
        PacHaut -> x, y+1
        | PacBas -> x, y-1
        | PacGauche -> x-1, y
        | PacDroite -> x+1, y in
    let oppose d1 d2 = match d1, d2 with
        PacHaut, PacBas -> true
        | PacBas, PacHaut -> true
        | PacGauche, PacDroite -> true
        | PacDroite, PacGauche -> true
        | _ -> false in
    let try_tourne = ref None in
    let tourne dir = 
        let px, py = pac.position in
        let tx,ty = px / tdim, py / tdim in
        let ntx, nty = deplace_dir (tx, ty) dir in
        if dir <> pac.direction && pac.status = Running 
        then begin
            (if est_sol ntx nty
            then begin
                try_tourne := None;
                pac.direction <- dir;
                if not (oppose dir pac.direction)
                then 
                    let px, py = pac.position in
                    let tx,ty = px / tdim, py / tdim in
                    let ntx, nty = deplace_dir (tx, ty) dir in
                    let npx, npy = ntx * tdim + tdim /2, nty * tdim + tdim/2 in
                    pac.status <- TurningTo (npx, npy)
            end else try_tourne := Some dir)
        end
    in
    set_draw_func ge
    (fun () -> 
        ()
    );
    let scatter n = (n mod (27 * 60)) < 7*60 in
    set_idle_func ge
    ( fun n -> 
        (match !try_tourne with
        None -> ()
        | Some d -> tourne d);

        let _ = List.map
            (fun pac ->
            match pac.status with
            Running -> begin
                let px, py = pac.position in
                let tx,ty = px / tdim, py / tdim in
                let npx, npy = deplace_dir pac.position pac.direction in
                let ntx, nty = deplace_dir (tx, ty) pac.direction in
                let ntx = if ntx < 0 then monde_largeur - 1 else if ntx > monde_largeur - 1 then 0 else ntx in
                let npx = if npx < 0 then 8 * monde_largeur - 1 else if npx > 8 * monde_largeur - 1 then 0 else npx in
                if est_sol ntx nty
                    || (pac.direction = PacHaut && py mod tdim < tdim/2)
                    || (pac.direction = PacBas && py mod tdim > tdim/2)
                    || (pac.direction = PacGauche && px mod tdim > tdim/2)
                    || (pac.direction = PacDroite && px mod tdim < tdim/2)
                then pac.position <- (npx,npy)
            end
            | TurningTo (npx,npy) -> begin
                let px, py = pac.position in
                let tx,ty = px / tdim, py / tdim in
                pac.position <-
                    ((if npx < px then px-1 else if npx > px then px+1 else px),
                     (if npy < py then py-1 else if npy > py then py+1 else py));
                if pac.position = (npx,npy)
                then pac.status <- Running
            end)
            [ pac; blinky; inky; pinky; clyde ]
        in
        let _ = List.map
            (fun (fantome,strat) ->
            let px, py = fantome.position in
            let tx, ty = px / tdim, py / tdim in
            let ppx, ppy = pac.position in
            let ptx, pty = ppx / tdim, ppy / tdim in
            if ptx = tx && pty = ty
            then (moveto 0 0; set_color red; draw_string (Printf.sprintf "GAME OVER     Score %d" ge.frames); wait_for_keypress (); stop_loop ge);
            if px mod tdim = tdim/2 && py mod tdim = tdim/2
            then begin
                let tx, ty = px / tdim, py / tdim in
                let cand = match fantome.direction with
                    | PacHaut -> [ PacHaut; PacDroite; PacGauche ]
                    | PacBas -> [ PacBas; PacDroite; PacGauche ]
                    | PacGauche -> [ PacHaut; PacBas; PacGauche ]
                    | PacDroite -> [ PacHaut; PacBas; PacDroite ] in
                let cand = List.map (fun d -> (d, deplace_dir (tx, ty) d)) cand in
                let valide = List.filter (fun (_,(tx,ty)) -> 
                    let tx = if tx < 0 then monde_largeur - 1 else if tx > monde_largeur - 1 then 0 else tx in
                    est_sol tx ty) cand in
                let nd = match valide with
                 [ d, _ ] -> d
                | _ -> strat (if scatter n then Scatter else Chase) monde pac (blinky,inky,pinky,clyde) valide in
                fantome.direction <- nd
            end
            )
            [ blinky, fb; inky, fi; pinky, fp; clyde, fc ]
        in ()
    );
    set_keypress_func ge 
    (function 
        | 'k' -> stop_loop ge
        | 'z' -> tourne PacHaut
        | 's' -> tourne PacBas
        | 'q' -> tourne PacGauche
        | 'd' -> tourne PacDroite
        | _ -> ()
    );
    set_tile_map_to_bitmap_id ge (fun di dj -> 
        if di >= 0 && di < monde_largeur && dj >= 0 && dj < monde_hauteur
        then Some (di + (monde_hauteur - dj - 1) * monde_largeur)
        else None);
    let offset d = match d with
        PacHaut -> 3 | PacBas -> 1 | PacDroite -> 0 | PacGauche -> 2 in
    set_get_sprites_count ge (fun () -> 5);
    set_get_sprite ge (fun i -> 
        match i with
        | 0 ->
            let px, py = pac.position in
            (cycle_ids ge (Array.map (fun n -> n+offset pac.direction * 3) pac_ids), 
            (px - 4, py - 8),
            true, true)
        | 1 ->
            let px, py = blinky.position in
            (cycle_ids ge (Array.map (fun n -> n+offset blinky.direction * 2) blinky_ids), 
            (px - 4, py - 8),
            true, true)
        | 2 ->
            let px, py = pinky.position in
            (cycle_ids ge (Array.map (fun n -> n+offset pinky.direction * 2) pinky_ids), 
            (px - 4, py - 8),
            true, true)
        | 3 ->
            let px, py = inky.position in
            (cycle_ids ge (Array.map (fun n -> n+offset inky.direction * 2) inky_ids), 
            (px - 4, py - 8),
            true, true)
        | _ ->
            let px, py = clyde.position in
            (cycle_ids ge (Array.map (fun n -> n+offset clyde.direction * 2) clyde_ids), 
            (px - 4, py - 8),
            true, true)
    );
    init_graphics_engine ge;
    main_loop ge

let lance_jeu j = match j with
    Pacman f -> pacman_lance f


let rec extract l lp n =
    match l with 
    | [] -> failwith "empty list"
    | t::q -> if n = 0 then (t,lp@q) else extract q (t::lp) (n-1)

let choice l = extract l [] (Random.int (List.length l))

let deplace mode monde pacman (blinky,pinky,inky,clyde) candidats =
    let (nd,_),_ = choice candidats in nd

let distance x0 y0 x1 y1 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1)

let target ptx pty l =
    let rec minl l = match l with
        [] -> failwith "Liste candidats vide"
        | [e] -> e
        | (nd,(tx,ty))::q -> let (mnd,(mtx,mty)) = minl q in
            if distance tx ty ptx pty < distance mtx mty ptx pty
            then (nd,(tx,ty))
            else (mnd, (mtx,mty))
    in let nd, _ = minl l in nd

let deplace_blinky mode monde pacman (blinky,pinky,inky,clyde) candidats =
    if mode = Scatter then target 27 35 candidats
    else 
        let px, py = pacman.position in
        let ptx, pty = px/8, py/8 in
        target ptx pty candidats
    

let deplace_pinky mode monde pacman (blinky,pinky,inky,clyde) candidats =
    if mode = Scatter then target 3 35 candidats
    else 
    let px, py = pacman.position in
    let ptx, pty = px/8, py/8 in
    let rtx, rty = 
        match pacman.direction with
        PacHaut -> ptx, pty + 4 
        | PacBas -> ptx, pty - 4
        | PacGauche -> ptx - 4, pty
        | PacDroite -> ptx + 4, pty
    in
    target rtx rty candidats

let deplace_inky mode monde pacman (blinky,pinky,inky,clyde) candidats =
    if mode = Scatter then target 27 (-1) candidats
    else 
    let px, py = pacman.position in
    let ptx, pty = px/8, py/8 in
    let rtx, rty = 
        match pacman.direction with
        PacHaut -> ptx, pty + 2 
        | PacBas -> ptx, pty - 2
        | PacGauche -> ptx - 2, pty
        | PacDroite -> ptx + 2, pty
    in
    let bx, by = blinky.position in
    let btx, bty = bx/8, by/8 in

    target (2 * rtx - btx) (2 * rty - bty) candidats

let deplace_clyde mode monde pacman (blinky,pinky,inky,clyde) candidats =
    if mode = Scatter then target 0 (-1) candidats
    else 
    let px, py = pacman.position in
    let ptx, pty = px/8, py/8 in
    let bx, by = clyde.position in
    let btx, bty = bx/8, by/8 in
    if distance ptx pty btx bty > 8 * 8
    then target ptx pty candidats
    else target 0 (-1) candidats


let _ =
    lance_jeu (Pacman (deplace_blinky, deplace_pinky, deplace_inky, deplace_clyde))
