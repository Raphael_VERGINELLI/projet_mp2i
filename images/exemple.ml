open Graphics
(* L'objectif ici est d'essayer de programmer une boucle de gestion
   des événements sans latence *)

let imagelib_vers_graphics im =
    let w, h = im.Image.width, im.Image.height in
    let m = Array.init h 
        (fun j ->
            Array.init w 
                (fun i -> Image.read_rgb im i j rgb)) in
    make_image m


let _ =
    open_graph " ";

    let bmp = ImageLib_unix.openfile "civ.bmp" in
    let bmp_gx = imagelib_vers_graphics bmp in
    let frame = ref 0 in
    let t_x = ref 250 in
    let t_y = ref 250 in
    let theta = ref 0. in
    let running = ref true in

    (* on va viser les 60 images secondes *)
    let minimal_frame_time = 1.0 /. 60. in
    (* on supprime la synchronisation automatique de l'écran avec le tampon *)
    auto_synchronize false;

    while !running do
        let start_time = Sys.time () in
        incr frame;
        clear_graph ();

        draw_image bmp_gx 0 0;

        set_color red;
        fill_circle !t_x !t_y 30;
        set_color black;
        draw_circle !t_x !t_y 30;

        theta := !theta +. 0.1;
        set_color blue;
        fill_circle (!t_x + int_of_float (50. *. cos !theta))
                    (!t_y + int_of_float (50. *. sin !theta)) 10;

        let process_key c = 
            match c with
            | 'k' -> running := false
            | 'a' -> t_x := !t_x - 2
            | 'd' -> t_x := !t_x + 2
            | 'w' -> t_y := !t_y + 2
            | 's' -> t_y := !t_y - 2
            | _ -> ()
        in
        let rec process_all_keys () =
            let st = wait_next_event [Poll] in
            if st.keypressed
            then begin
                let stk = wait_next_event [Key_pressed] in
                process_key stk.key;
                process_all_keys ()

            end
        in
        process_all_keys();

        (* on rafraichit l'écran *)
        synchronize ();

        (* Une attente active si on va trop vite *)
        let t = Sys.time () in
        let dt = start_time +. minimal_frame_time -. t in
        if dt > 0.
        then Unix.sleepf dt
    done

